<?php

namespace Esalazarv\Resource\Test;

use Esalazarv\Resource\ApiResponse;
use PHPUnit_Framework_TestCase;

class ApiResponseTest extends PHPUnit_Framework_TestCase
{

    protected  $data = [
        'headers' => [
            'content-type' => 'json'
        ],
        'status' => [
            'code' => 200,
            'message' => 'ok',
        ],
        'body'  => [
            'name' => 'Edcilo'
        ],
        'cache' => [
            'enabled' => false,
            'created_at' => null,
            'end_at' => null,
        ]
    ];

    /**
     * Proves that an ApiResponse object has attributes property.
     *
     * @return void
     */
    public function testResponseHasAttributes()
    {
        $apiResponse = new ApiResponse($this->data);

        $this->assertObjectHasAttribute('attributes', $apiResponse);
    }

    /**
     * Proves that an ApiResponse object can get data of attributes porperty.
     *
     * @return void
     */
    public function testResponseFindDataWithMagicMethod()
    {
        $response = new ApiResponse($this->data);
        $this->assertArrayHasKey('content-type', $response->headers());
        $this->assertEquals(200, $response->status('code'));
    }

    /**
     * Proves that an ApiResponse object returns a JSON string of body
     *
     * @return void
     */
    public function testResponseStringConversionReturnBodyJson()
    {
        $response = new ApiResponse($this->data);
        $this->assertJsonStringEqualsJsonString((string)$response, json_encode($this->data['body']));
    }

    /**
     * Proves that an ApiResponse object can iterate on body
     */
    public function testResponseIteration()
    {
        $response = new ApiResponse($this->data);
        $this->assertEquals(1,count($response));
    }
}
