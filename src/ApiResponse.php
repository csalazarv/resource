<?php

namespace Esalazarv\Resource;

class ApiResponse implements \IteratorAggregate
{
    /**
     * This build a response with an array data
     *
     * @param array $attributes
     */
    public function __construct($attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * This find data in attributes
     *
     * @param $name
     * @param array $params
     * @return mixed
     */
    public function __call($name, $params = [])
    {
        return empty($params) ? $this->attributes[$name] : $this->attributes[$name][$params[0]];
    }

    /**
     * This make a string for a instace of this class
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->attributes['body']);
    }

    /**
     * Allow itarete for a instance of this class
     *
     * @return mixed
     */
    public function getIterator() {
        return $this->attributes['body'];
    }
}