<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Api Endpoint Resources
    |--------------------------------------------------------------------------
    |
    |This store a endpoint list, thi can to be an array or a string.
    |
    */
    'endpoint' => '',

    /*
    |--------------------------------------------------------------------------
    | Api Responce Enable Cache
    |--------------------------------------------------------------------------
    |
    |This enable or disable cache storage.
    |
    */
    'enable_cache' => false,

    /*
    |--------------------------------------------------------------------------
    | Api cache response live time
    |--------------------------------------------------------------------------
    |
    |This set time live for cache storage in minutes.
    |
    */
    'cache_live_time' => 0,

    /*
    |--------------------------------------------------------------------------
    | Api wrapper for collections
    |--------------------------------------------------------------------------
    |
    |This set a wrapper for responses.
    |
    */
    'wrapper' => null,

    /*
    |--------------------------------------------------------------------------
    | Api http errors exceptions
    |--------------------------------------------------------------------------
    |
    |This enable or disable exceptions for http errors.
    |
    */
    'http_errors' => false,
];