<?php

namespace Esalazarv\Resource;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

trait ResourceTrait
{

    /**
     * This get a endpoint for a model
     *
     * @return GuzzleHttp\Client
     */
    private function endpoint()
    {
        $endpoint = $this->config['endpoint'];
        if (!isset($this->endpoint)) {
            $endpoint = is_array($endpoint) ? array_first($endpoint) : $endpoint;
        } else {
            $endpoint = $endpoint[$this->endpoint];
        }
        $this->apiHeaders = [];
        return new Client([
            'base_uri' => $endpoint
        ]);
    }

    /**
     * This set headers for a request
     *
     * @param array $headers
     * @return $this
     */
    public function headers($headers = [])
    {
        $this->apiHeaders = array_merge($this->apiHeaders, $headers);
        return $this;
    }

    /**
     * Magic method for make a request using a http verb (GET, POST, PUT, PATCH, DELETE ...)
     *
     * @param string $name
     * @param array $params
     * @return GuzzleHttp\Psr7\Response
     */
    public function __call($name, $params = [])
    {
        array_unshift($params, $name);
        return call_user_func_array(array($this, "request"), $params);
    }

    /**
     * This send request
     *
     * @param string $method
     * @param string $uri
     * @param array $params
     * @param null|integer $cache
     * @return Esalazarv\Resource\ApiResponse
     */
    public function request($method, $uri, $params, $cache = null)
    {
        $this->config = config('resource');
        $name = str_slug($method.$uri.serialize($this->apiHeaders).serialize($params));

        $enableCache = is_null($cache) ? $this->config['enable_cache'] : $cache;
        if ($enableCache && Cache::has($name)) {
            $data = Cache::get($name);
        } else {
            $dataSend = [
                'headers' => $this->apiHeaders,
                'http_errors' =>  $this->config['http_errors'],
            ];
            switch($method) {
                case 'get' :
                    $dataSend['query'] = $params;
                    break;
                case 'post': ;
                case 'put' : ;
                case 'patch':
                    $dataSend['form_params'] = $params;
                    break;
                default:
                    $dataSend['body'] = $params;
            };
            $serverResponse = $this->endpoint()->request($method, $uri , $dataSend);
            $data = $this->makeData($serverResponse);
        }

        $this->setInCache($name, $data, $cache);

        $response = $this->prepareResponse($data);

        return new ApiResponse($response);
    }

    /**
     * This build a data response
     *
     * @param GuzzleHttp\Psr7\Response $serverResponse
     * @return array
     */
    private function makeData(Response $serverResponse)
    {
        return [
            'headers' => $serverResponse->getHeaders(),
            'status' => [
                'code' => $serverResponse->getStatusCode(),
                'message' => $serverResponse->getReasonPhrase(),
            ],
            'body'  => json_decode($serverResponse->getBody()),
            'cache' => [
                'enabled' => false,
                'created_at' => null,
                'end_at' => null,
            ]
        ];
    }

    /**
     * This store a response in cache
     *
     * @param string $name
     * @param array $data
     * @param null|integer $cache
     */
    private function setInCache($name, $data, $cache = null)
    {
        if ($this->config['enable_cache']) {
            $lifeTime = is_null($cache) ? $this->config['cache_live_time'] : $cache;
            $created_at = Carbon::now();
            $data['cache']['enabled'] = true;
            $data['cache']['created_at'] = $created_at->timestamp;
            $data['cache']['end_at'] = $created_at->addMinutes($lifeTime)->timestamp;
            Cache::add($name, $data, $lifeTime);
        }
    }

    /**
     * This prepare data to build ApiResponse
     *
     * @param array $data
     * @return array
     */
    private function prepareResponse($data)
    {
        $wrapper= $this->config['wrapper'];
        $body = (!is_null($wrapper) && isset($data['body']->$wrapper)) ? collect($data['body']->$wrapper) : collect($data['body']);
        $keys = array_keys($body->toArray());
        $fillables = array_keys($this->fillable);
        $exists = array_intersect($keys, $fillables);
        if (count($exists) > 0) {
            if (isset($keys[0]) && is_numeric($keys[0])) {
                $body = collect(json_decode($body))->map(function ($item) {
                    $class = get_class($this);
                    return new $class((array)$item);
                });
            } else {
                $class = get_class($this);
                $body = collect([new $class($body->toArray())]);
            }
        } else {
            $data['metadata'] = $body;
            $body = collect([]);
        }

        $data['body'] = ($body->count() > 1) ? $body : $body->first();
        $data['cache']['created_at'] = is_null($data['cache']['created_at'])
            ? null
            : Carbon::createFromTimestamp($data['cache']['created_at']);
        $data['cache']['end_at'] = is_null($data['cache']['end_at'])
            ? null
            : Carbon::createFromTimestamp($data['cache']['end_at']);
        return $data;
    }
}
