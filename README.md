# resource
Pakage for consume API resources for Laravel

## Requeriments

* PHP >=5.5.9
* Laravel 5.*


##Composer Installation

The best way to install it is quickly and easily with Composer.

To install the most recent version, run the following command.

`
composer require esalazarv/resource
`

Now your composer.json has been updated automatically and you're able to require the just created vendor/autoload.php file to PSR-4 autoload the library.


After you have installed, open your Laravel config file `config/app.php and add the following lines.`

```
Esalazarv\Resource\ApiResponseServiceProvider::class
```

###Configuration

Publish configuration in Laravel 5

```
php artisan vendor:publish --provider="Esalazarv\Resource\ApiResponseServiceProvider"
```

###Usage

For use this package you most include Resource trait in your models.


```
use Esalazarv\Resource\ResourceTrait;


class MyAwesomeModel extends \Illuminate\Database\Eloquent\Model
{
    use ResourceTrait;

    protected $fillable = ["id", "name", "description"];
    
    protected $endpoint = "my-awesome-endpoint";
    
    protected $apiHeaders = [
        "token" => 0123456789
    ];

    public function getAll(Request $request)
    {
        return $this->get("users", $request->all());
    }
}
```